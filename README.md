[![输开源协议](https://img.shields.io/badge/License-Apache--2.0-brightgreen.svg "Apache")](https://www.apache.org/licenses/LICENSE-2.0)

[![maven最新版本](https://maven-badges.herokuapp.com/maven-central/org.t-io/tio-core/badge.svg "maven最新版本")](https://maven-badges.herokuapp.com/maven-central/org.t-io/tio-core)

## **t-io: 让网络编程更轻松和有趣**

[![image](https://gitee.com/tywo45/t-io/raw/master/docs/logo/preview.png)](http://www.t-io.org/#/doc)




## **t-io诞生的意义**
- 旧时王谢堂前燕，飞入寻常百姓家----当年那些王谢贵族们才拥有的"百万级即时通讯"应用，将因为t-io的诞生，纷纷飞入普通人家的屋檐下。

### **[t-io文档](http://t-io.org/#/doc "t-io文档")**





